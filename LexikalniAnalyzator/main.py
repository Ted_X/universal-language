import sys
import math

from antlr4 import *
from dist.VCalculatorLexer import VCalculatorLexer
from dist.VCalculatorParser import VCalculatorParser
from dist.VCalculatorVisitor import VCalculatorVisitor

class CalcVisitor(VCalculatorVisitor):
    def visitAtomExpr(self, ctx: VCalculatorParser.AtomExprContext):
        if '!' in ctx.getText():
            num = ctx.getText().replace('!','')    
            if ctx.getText().startswith('0b'):
                return math.factorial(int(num, 2))
            elif ctx.getText().startswith('0x') or ctx.getText().startswith('0X'):
                return math.factorial(int(num, 16))
            return math.factorial(int(num))
        if ctx.getText().startswith('0b'):
            return int(ctx.getText(), 2)
        elif ctx.getText().startswith('0x') or ctx.getText().startswith('0X'):
            return int(ctx.getText(), 16)
        return int(ctx.getText())
    
    def visitParenExpr(self, ctx: VCalculatorParser.ParenExprContext):
        return self.visit(ctx.expr())
    
    def visitOpExpr(self, ctx: VCalculatorParser.OpExprContext):
        l = self.visit(ctx.left)
        r = self.visit(ctx.right)

        op = ctx.op.text
        if op == '+':
            return l + r
        elif op == '-':
            return l - r
        elif op == '*':
            return l * r
        elif op == '/':
            if r == 0:
                print('divide by zero!')
                return 0
            return l / r
        
def calc(line) -> float:
    input_stream = InputStream(line)

    # lexing
    lexer = VCalculatorLexer(input_stream)
    stream = CommonTokenStream(lexer)

    # parsing
    parser = VCalculatorParser(stream)
    tree = parser.expr()
    
    if (VCalculatorParser.isValid == 'CI'): 
        # use customized visitor to traverse AST
        visitor = CalcVisitor()
        return visitor.visit(tree)
    return 'WI'



if __name__ == '__main__':
    example = []
    result = []
    expected = []
    with open(sys.argv[1], 'r') as test_file:
        lines = test_file.readlines()
        for line in lines:
            example.append(line.strip())
            tmp = calc(line.strip())
            if (tmp != 'WI'):
                result.append(tmp)
            else:
                result.append('Wrong input')

    with open(sys.argv[2], 'r') as expected_result:
        lines = expected_result.readlines()
        for line in lines:
            expected.append(line.strip())
    correct = 0
    print('\n\n##############################')
    print('_______VASKO CALCULATOR_______')
    print('##############################')
    for i in range(len(expected)):
        if ((
            result[i] == 'Wrong input' and 
            expected[i] == 'Wrong input') or 
            float(result[i]) == float(expected[i])):
            
            print(f'Example {i+1}: {example[i]} = {result[i]}')
            print(f'EXPECTED: {expected[i]}')
            print('CORRECT')
            print('------------------------------')
            correct += 1
        else:
            print(f'Example {i+1}: {example[i]} = {result[i]}')
            print(f'EXPECTED: {expected[i]}')
            print('INCORRECT')
            print('------------------------------')
    print('##############################')
    print('____________RESULT____________')    
    percent = (correct/len(result))*100
    print(f'{correct}/{len(result)} => {round(percent, 2)}%')
    print('##############################')
