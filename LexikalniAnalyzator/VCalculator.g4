grammar VCalculator;
calculate : calc*;
calc : expr;

expr:	left=expr op=('*'|'/') right=expr  # OpExpr
    |	left=expr op=('+'|'-') right=expr  # OpExpr
    |	atom=NUMBER                        # AtomExpr
    |	'(' expr ')'                       # ParenExpr
    ;

/*NUMBER
    : INT FAC
    | BIN FAC
    | HEX FAC
    ;
INT: [0-9]+([0-9]+)*;
BIN: [0][b|B][01]+;
HEX: [0][x|X][0-9a-fA-F]+;
*/

NUMBER
    : DECIMAL_INTEGER FAC
    | BIN_INTEGER FAC
    | HEX_INTEGER FAC
    ;



DECIMAL_INTEGER
    : NON_ZERO_DIGIT DIGIT*
    | '0'+
    ;

HEX_INTEGER
    : '0' [xX] HEX_DIGIT*
    ;

BIN_INTEGER
    : '0' [bB] BIN_DIGIT*
    ;


// Nonzerodigit ::= '1'...'9'
NON_ZERO_DIGIT
    : [1-9]
    ;
// Digit ::= '0'...'9'
DIGIT
    : [0-9]
    ;
// Hexdigit ::= digit | 'a'...'f' | 'A'...'F'
HEX_DIGIT
    : [0-9a-fA-F]
    ;
// Bindigit ::= '0' | '1'
BIN_DIGIT
    : [01]
    ;

FAC: '!'|;
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines