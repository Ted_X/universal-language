import os

os.system('''
#sudo apt-get install default-jdk
#cd /usr/local/lib
#sudo curl -O https://www.antlr.org/download/antlr-4.9.2-complete.jar
export CLASSPATH=".:/usr/local/lib/antlr-4.9.2-complete.jar:$CLASSPATH"
alias antlr4='java -jar /usr/local/lib/antlr-4.9.2-complete.jar'
alias grun='java org.antlr.v4.gui.TestRig'
cd /Users/tadeasvasko/Documents/School/PRK/LAGraf
antlr4 VCalculator.g4
javac VCalculator*.java
grun VCalculator calculate testValid -tree -gui
cd /Users/tadeasvasko/Documents/School/PRK/LexikalniAnalyzator
virtualenv venv
source venv/bin/activate
pip install antlr4-python3-runtime==4.9.2
antlr4 -Dlanguage=Python3 VCalculator.g4 -visitor -o dist
''')