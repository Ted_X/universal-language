# Generated from VCalculator.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .VCalculatorParser import VCalculatorParser
else:
    from VCalculatorParser import VCalculatorParser

# This class defines a complete listener for a parse tree produced by VCalculatorParser.
class VCalculatorListener(ParseTreeListener):

    # Enter a parse tree produced by VCalculatorParser#calculate.
    def enterCalculate(self, ctx:VCalculatorParser.CalculateContext):
        pass

    # Exit a parse tree produced by VCalculatorParser#calculate.
    def exitCalculate(self, ctx:VCalculatorParser.CalculateContext):
        pass


    # Enter a parse tree produced by VCalculatorParser#calc.
    def enterCalc(self, ctx:VCalculatorParser.CalcContext):
        pass

    # Exit a parse tree produced by VCalculatorParser#calc.
    def exitCalc(self, ctx:VCalculatorParser.CalcContext):
        pass


    # Enter a parse tree produced by VCalculatorParser#AtomExpr.
    def enterAtomExpr(self, ctx:VCalculatorParser.AtomExprContext):
        pass

    # Exit a parse tree produced by VCalculatorParser#AtomExpr.
    def exitAtomExpr(self, ctx:VCalculatorParser.AtomExprContext):
        pass


    # Enter a parse tree produced by VCalculatorParser#ParenExpr.
    def enterParenExpr(self, ctx:VCalculatorParser.ParenExprContext):
        pass

    # Exit a parse tree produced by VCalculatorParser#ParenExpr.
    def exitParenExpr(self, ctx:VCalculatorParser.ParenExprContext):
        pass


    # Enter a parse tree produced by VCalculatorParser#OpExpr.
    def enterOpExpr(self, ctx:VCalculatorParser.OpExprContext):
        pass

    # Exit a parse tree produced by VCalculatorParser#OpExpr.
    def exitOpExpr(self, ctx:VCalculatorParser.OpExprContext):
        pass



del VCalculatorParser