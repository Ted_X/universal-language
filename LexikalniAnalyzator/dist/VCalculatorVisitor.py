# Generated from VCalculator.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .VCalculatorParser import VCalculatorParser
else:
    from VCalculatorParser import VCalculatorParser

# This class defines a complete generic visitor for a parse tree produced by VCalculatorParser.

class VCalculatorVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by VCalculatorParser#calculate.
    def visitCalculate(self, ctx:VCalculatorParser.CalculateContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by VCalculatorParser#calc.
    def visitCalc(self, ctx:VCalculatorParser.CalcContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by VCalculatorParser#AtomExpr.
    def visitAtomExpr(self, ctx:VCalculatorParser.AtomExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by VCalculatorParser#ParenExpr.
    def visitParenExpr(self, ctx:VCalculatorParser.ParenExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by VCalculatorParser#OpExpr.
    def visitOpExpr(self, ctx:VCalculatorParser.OpExprContext):
        return self.visitChildren(ctx)



del VCalculatorParser