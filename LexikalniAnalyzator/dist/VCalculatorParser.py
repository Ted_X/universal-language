# Generated from VCalculator.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\22")
        buf.write("$\4\2\t\2\4\3\t\3\4\4\t\4\3\2\7\2\n\n\2\f\2\16\2\r\13")
        buf.write("\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\5\4\27\n\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\7\4\37\n\4\f\4\16\4\"\13\4\3\4\2\3\6")
        buf.write("\5\2\4\6\2\4\3\2\3\4\3\2\5\6\2$\2\13\3\2\2\2\4\16\3\2")
        buf.write("\2\2\6\26\3\2\2\2\b\n\5\4\3\2\t\b\3\2\2\2\n\r\3\2\2\2")
        buf.write("\13\t\3\2\2\2\13\f\3\2\2\2\f\3\3\2\2\2\r\13\3\2\2\2\16")
        buf.write("\17\5\6\4\2\17\5\3\2\2\2\20\21\b\4\1\2\21\27\7\t\2\2\22")
        buf.write("\23\7\7\2\2\23\24\5\6\4\2\24\25\7\b\2\2\25\27\3\2\2\2")
        buf.write("\26\20\3\2\2\2\26\22\3\2\2\2\27 \3\2\2\2\30\31\f\6\2\2")
        buf.write("\31\32\t\2\2\2\32\37\5\6\4\7\33\34\f\5\2\2\34\35\t\3\2")
        buf.write("\2\35\37\5\6\4\6\36\30\3\2\2\2\36\33\3\2\2\2\37\"\3\2")
        buf.write("\2\2 \36\3\2\2\2 !\3\2\2\2!\7\3\2\2\2\" \3\2\2\2\6\13")
        buf.write("\26\36 ")
        return buf.getvalue()


class VCalculatorParser ( Parser ):

    isValid = 'CI'

    grammarFileName = "VCalculator.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'*'", "'/'", "'+'", "'-'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "NUMBER", "DECIMAL_INTEGER", 
                      "HEX_INTEGER", "BIN_INTEGER", "NON_ZERO_DIGIT", "DIGIT", 
                      "HEX_DIGIT", "BIN_DIGIT", "FAC", "WS" ]

    RULE_calculate = 0
    RULE_calc = 1
    RULE_expr = 2

    ruleNames =  [ "calculate", "calc", "expr" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    NUMBER=7
    DECIMAL_INTEGER=8
    HEX_INTEGER=9
    BIN_INTEGER=10
    NON_ZERO_DIGIT=11
    DIGIT=12
    HEX_DIGIT=13
    BIN_DIGIT=14
    FAC=15
    WS=16

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class CalculateContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def calc(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(VCalculatorParser.CalcContext)
            else:
                return self.getTypedRuleContext(VCalculatorParser.CalcContext,i)


        def getRuleIndex(self):
            return VCalculatorParser.RULE_calculate

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCalculate" ):
                listener.enterCalculate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCalculate" ):
                listener.exitCalculate(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCalculate" ):
                return visitor.visitCalculate(self)
            else:
                return visitor.visitChildren(self)




    def calculate(self):

        localctx = VCalculatorParser.CalculateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_calculate)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 9
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==VCalculatorParser.T__4 or _la==VCalculatorParser.NUMBER:
                self.state = 6
                self.calc()
                self.state = 11
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CalcContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(VCalculatorParser.ExprContext,0)


        def getRuleIndex(self):
            return VCalculatorParser.RULE_calc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCalc" ):
                listener.enterCalc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCalc" ):
                listener.exitCalc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCalc" ):
                return visitor.visitCalc(self)
            else:
                return visitor.visitChildren(self)




    def calc(self):

        localctx = VCalculatorParser.CalcContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_calc)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 12
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return VCalculatorParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class AtomExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a VCalculatorParser.ExprContext
            super().__init__(parser)
            self.atom = None # Token
            self.copyFrom(ctx)

        def NUMBER(self):
            return self.getToken(VCalculatorParser.NUMBER, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtomExpr" ):
                listener.enterAtomExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtomExpr" ):
                listener.exitAtomExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtomExpr" ):
                return visitor.visitAtomExpr(self)
            else:
                return visitor.visitChildren(self)


    class ParenExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a VCalculatorParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(VCalculatorParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenExpr" ):
                listener.enterParenExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenExpr" ):
                listener.exitParenExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParenExpr" ):
                return visitor.visitParenExpr(self)
            else:
                return visitor.visitChildren(self)


    class OpExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a VCalculatorParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(VCalculatorParser.ExprContext)
            else:
                return self.getTypedRuleContext(VCalculatorParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOpExpr" ):
                listener.enterOpExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOpExpr" ):
                listener.exitOpExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOpExpr" ):
                return visitor.visitOpExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = VCalculatorParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 4
        self.enterRecursionRule(localctx, 4, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 20
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [VCalculatorParser.NUMBER]:
                localctx = VCalculatorParser.AtomExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 15
                localctx.atom = self.match(VCalculatorParser.NUMBER)
                pass
            elif token in [VCalculatorParser.T__4]:
                localctx = VCalculatorParser.ParenExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 16
                self.match(VCalculatorParser.T__4)
                self.state = 17
                self.expr(0)
                self.state = 18
                self.match(VCalculatorParser.T__5)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 30
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,3,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 28
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
                    if la_ == 1:
                        localctx = VCalculatorParser.OpExprContext(self, VCalculatorParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 22
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 23
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==VCalculatorParser.T__0 or _la==VCalculatorParser.T__1):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 24
                        localctx.right = self.expr(5)
                        pass

                    elif la_ == 2:
                        localctx = VCalculatorParser.OpExprContext(self, VCalculatorParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 25
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 26
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==VCalculatorParser.T__2 or _la==VCalculatorParser.T__3):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 27
                        localctx.right = self.expr(4)
                        pass

             
                self.state = 32
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,3,self._ctx)

        except RecognitionException as re:
            VCalculatorParser.isValid = 'WI'
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[2] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 3)
         




