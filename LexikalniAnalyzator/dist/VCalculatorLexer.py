# Generated from VCalculator.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\22")
        buf.write("k\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2\3\3\3\3\3")
        buf.write("\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\5\b9\n\b\3\t\3\t\7\t=\n\t\f\t\16\t@\13\t")
        buf.write("\3\t\6\tC\n\t\r\t\16\tD\5\tG\n\t\3\n\3\n\3\n\7\nL\n\n")
        buf.write("\f\n\16\nO\13\n\3\13\3\13\3\13\7\13T\n\13\f\13\16\13W")
        buf.write("\13\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\5")
        buf.write("\20c\n\20\3\21\6\21f\n\21\r\21\16\21g\3\21\3\21\2\2\22")
        buf.write("\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31")
        buf.write("\16\33\17\35\20\37\21!\22\3\2\t\4\2ZZzz\4\2DDdd\3\2\63")
        buf.write(";\3\2\62;\5\2\62;CHch\3\2\62\63\5\2\13\f\17\17\"\"\2s")
        buf.write("\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13")
        buf.write("\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3")
        buf.write("\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2")
        buf.write("\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\3#\3\2\2\2\5")
        buf.write("%\3\2\2\2\7\'\3\2\2\2\t)\3\2\2\2\13+\3\2\2\2\r-\3\2\2")
        buf.write("\2\178\3\2\2\2\21F\3\2\2\2\23H\3\2\2\2\25P\3\2\2\2\27")
        buf.write("X\3\2\2\2\31Z\3\2\2\2\33\\\3\2\2\2\35^\3\2\2\2\37b\3\2")
        buf.write("\2\2!e\3\2\2\2#$\7,\2\2$\4\3\2\2\2%&\7\61\2\2&\6\3\2\2")
        buf.write("\2\'(\7-\2\2(\b\3\2\2\2)*\7/\2\2*\n\3\2\2\2+,\7*\2\2,")
        buf.write("\f\3\2\2\2-.\7+\2\2.\16\3\2\2\2/\60\5\21\t\2\60\61\5\37")
        buf.write("\20\2\619\3\2\2\2\62\63\5\25\13\2\63\64\5\37\20\2\649")
        buf.write("\3\2\2\2\65\66\5\23\n\2\66\67\5\37\20\2\679\3\2\2\28/")
        buf.write("\3\2\2\28\62\3\2\2\28\65\3\2\2\29\20\3\2\2\2:>\5\27\f")
        buf.write("\2;=\5\31\r\2<;\3\2\2\2=@\3\2\2\2><\3\2\2\2>?\3\2\2\2")
        buf.write("?G\3\2\2\2@>\3\2\2\2AC\7\62\2\2BA\3\2\2\2CD\3\2\2\2DB")
        buf.write("\3\2\2\2DE\3\2\2\2EG\3\2\2\2F:\3\2\2\2FB\3\2\2\2G\22\3")
        buf.write("\2\2\2HI\7\62\2\2IM\t\2\2\2JL\5\33\16\2KJ\3\2\2\2LO\3")
        buf.write("\2\2\2MK\3\2\2\2MN\3\2\2\2N\24\3\2\2\2OM\3\2\2\2PQ\7\62")
        buf.write("\2\2QU\t\3\2\2RT\5\35\17\2SR\3\2\2\2TW\3\2\2\2US\3\2\2")
        buf.write("\2UV\3\2\2\2V\26\3\2\2\2WU\3\2\2\2XY\t\4\2\2Y\30\3\2\2")
        buf.write("\2Z[\t\5\2\2[\32\3\2\2\2\\]\t\6\2\2]\34\3\2\2\2^_\t\7")
        buf.write("\2\2_\36\3\2\2\2`c\7#\2\2ac\3\2\2\2b`\3\2\2\2ba\3\2\2")
        buf.write("\2c \3\2\2\2df\t\b\2\2ed\3\2\2\2fg\3\2\2\2ge\3\2\2\2g")
        buf.write("h\3\2\2\2hi\3\2\2\2ij\b\21\2\2j\"\3\2\2\2\13\28>DFMUb")
        buf.write("g\3\b\2\2")
        return buf.getvalue()


class VCalculatorLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    NUMBER = 7
    DECIMAL_INTEGER = 8
    HEX_INTEGER = 9
    BIN_INTEGER = 10
    NON_ZERO_DIGIT = 11
    DIGIT = 12
    HEX_DIGIT = 13
    BIN_DIGIT = 14
    FAC = 15
    WS = 16

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'*'", "'/'", "'+'", "'-'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "NUMBER", "DECIMAL_INTEGER", "HEX_INTEGER", "BIN_INTEGER", "NON_ZERO_DIGIT", 
            "DIGIT", "HEX_DIGIT", "BIN_DIGIT", "FAC", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "NUMBER", 
                  "DECIMAL_INTEGER", "HEX_INTEGER", "BIN_INTEGER", "NON_ZERO_DIGIT", 
                  "DIGIT", "HEX_DIGIT", "BIN_DIGIT", "FAC", "WS" ]

    grammarFileName = "VCalculator.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


