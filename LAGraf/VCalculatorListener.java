// Generated from VCalculator.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link VCalculatorParser}.
 */
public interface VCalculatorListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#calculate}.
	 * @param ctx the parse tree
	 */
	void enterCalculate(VCalculatorParser.CalculateContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#calculate}.
	 * @param ctx the parse tree
	 */
	void exitCalculate(VCalculatorParser.CalculateContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#calc}.
	 * @param ctx the parse tree
	 */
	void enterCalc(VCalculatorParser.CalcContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#calc}.
	 * @param ctx the parse tree
	 */
	void exitCalc(VCalculatorParser.CalcContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(VCalculatorParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(VCalculatorParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#second}.
	 * @param ctx the parse tree
	 */
	void enterSecond(VCalculatorParser.SecondContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#second}.
	 * @param ctx the parse tree
	 */
	void exitSecond(VCalculatorParser.SecondContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#first}.
	 * @param ctx the parse tree
	 */
	void enterFirst(VCalculatorParser.FirstContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#first}.
	 * @param ctx the parse tree
	 */
	void exitFirst(VCalculatorParser.FirstContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#prio}.
	 * @param ctx the parse tree
	 */
	void enterPrio(VCalculatorParser.PrioContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#prio}.
	 * @param ctx the parse tree
	 */
	void exitPrio(VCalculatorParser.PrioContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#expr1}.
	 * @param ctx the parse tree
	 */
	void enterExpr1(VCalculatorParser.Expr1Context ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#expr1}.
	 * @param ctx the parse tree
	 */
	void exitExpr1(VCalculatorParser.Expr1Context ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#fac}.
	 * @param ctx the parse tree
	 */
	void enterFac(VCalculatorParser.FacContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#fac}.
	 * @param ctx the parse tree
	 */
	void exitFac(VCalculatorParser.FacContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(VCalculatorParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(VCalculatorParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#nonzeroN}.
	 * @param ctx the parse tree
	 */
	void enterNonzeroN(VCalculatorParser.NonzeroNContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#nonzeroN}.
	 * @param ctx the parse tree
	 */
	void exitNonzeroN(VCalculatorParser.NonzeroNContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#natural}.
	 * @param ctx the parse tree
	 */
	void enterNatural(VCalculatorParser.NaturalContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#natural}.
	 * @param ctx the parse tree
	 */
	void exitNatural(VCalculatorParser.NaturalContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#nonzeroH}.
	 * @param ctx the parse tree
	 */
	void enterNonzeroH(VCalculatorParser.NonzeroHContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#nonzeroH}.
	 * @param ctx the parse tree
	 */
	void exitNonzeroH(VCalculatorParser.NonzeroHContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#hexa}.
	 * @param ctx the parse tree
	 */
	void enterHexa(VCalculatorParser.HexaContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#hexa}.
	 * @param ctx the parse tree
	 */
	void exitHexa(VCalculatorParser.HexaContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#nonzeroB}.
	 * @param ctx the parse tree
	 */
	void enterNonzeroB(VCalculatorParser.NonzeroBContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#nonzeroB}.
	 * @param ctx the parse tree
	 */
	void exitNonzeroB(VCalculatorParser.NonzeroBContext ctx);
	/**
	 * Enter a parse tree produced by {@link VCalculatorParser#binary}.
	 * @param ctx the parse tree
	 */
	void enterBinary(VCalculatorParser.BinaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link VCalculatorParser#binary}.
	 * @param ctx the parse tree
	 */
	void exitBinary(VCalculatorParser.BinaryContext ctx);
}