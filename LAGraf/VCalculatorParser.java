// Generated from VCalculator.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class VCalculatorParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, WS=30;
	public static final int
		RULE_calculate = 0, RULE_calc = 1, RULE_expr = 2, RULE_second = 3, RULE_first = 4, 
		RULE_prio = 5, RULE_expr1 = 6, RULE_fac = 7, RULE_number = 8, RULE_nonzeroN = 9, 
		RULE_natural = 10, RULE_nonzeroH = 11, RULE_hexa = 12, RULE_nonzeroB = 13, 
		RULE_binary = 14;
	private static String[] makeRuleNames() {
		return new String[] {
			"calculate", "calc", "expr", "second", "first", "prio", "expr1", "fac", 
			"number", "nonzeroN", "natural", "nonzeroH", "hexa", "nonzeroB", "binary"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "';'", "'+'", "'-'", "'*'", "'/'", "'('", "')'", "'!'", "'0'", 
			"'0x'", "'0x0'", "'b'", "'b0'", "'B'", "'B0'", "'1'", "'2'", "'3'", "'4'", 
			"'5'", "'6'", "'7'", "'8'", "'9'", "'A'", "'C'", "'D'", "'E'", "'F'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "VCalculator.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public VCalculatorParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class CalculateContext extends ParserRuleContext {
		public List<CalcContext> calc() {
			return getRuleContexts(CalcContext.class);
		}
		public CalcContext calc(int i) {
			return getRuleContext(CalcContext.class,i);
		}
		public CalculateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calculate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterCalculate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitCalculate(this);
		}
	}

	public final CalculateContext calculate() throws RecognitionException {
		CalculateContext _localctx = new CalculateContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_calculate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23))) != 0)) {
				{
				{
				setState(30);
				calc();
				}
				}
				setState(35);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CalcContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public CalcContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterCalc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitCalc(this);
		}
	}

	public final CalcContext calc() throws RecognitionException {
		CalcContext _localctx = new CalcContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_calc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(36);
			expr();
			setState(37);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public FirstContext first() {
			return getRuleContext(FirstContext.class,0);
		}
		public SecondContext second() {
			return getRuleContext(SecondContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(39);
			first();
			setState(40);
			second();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SecondContext extends ParserRuleContext {
		public FirstContext first() {
			return getRuleContext(FirstContext.class,0);
		}
		public SecondContext second() {
			return getRuleContext(SecondContext.class,0);
		}
		public SecondContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_second; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterSecond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitSecond(this);
		}
	}

	public final SecondContext second() throws RecognitionException {
		SecondContext _localctx = new SecondContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_second);
		try {
			setState(51);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				enterOuterAlt(_localctx, 1);
				{
				setState(42);
				match(T__1);
				setState(43);
				first();
				setState(44);
				second();
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 2);
				{
				setState(46);
				match(T__2);
				setState(47);
				first();
				setState(48);
				second();
				}
				break;
			case T__0:
			case T__6:
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FirstContext extends ParserRuleContext {
		public Expr1Context expr1() {
			return getRuleContext(Expr1Context.class,0);
		}
		public PrioContext prio() {
			return getRuleContext(PrioContext.class,0);
		}
		public FirstContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_first; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterFirst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitFirst(this);
		}
	}

	public final FirstContext first() throws RecognitionException {
		FirstContext _localctx = new FirstContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_first);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			expr1();
			setState(54);
			prio();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrioContext extends ParserRuleContext {
		public Expr1Context expr1() {
			return getRuleContext(Expr1Context.class,0);
		}
		public PrioContext prio() {
			return getRuleContext(PrioContext.class,0);
		}
		public PrioContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prio; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterPrio(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitPrio(this);
		}
	}

	public final PrioContext prio() throws RecognitionException {
		PrioContext _localctx = new PrioContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_prio);
		try {
			setState(65);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				enterOuterAlt(_localctx, 1);
				{
				setState(56);
				match(T__3);
				setState(57);
				expr1();
				setState(58);
				prio();
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				setState(60);
				match(T__4);
				setState(61);
				expr1();
				setState(62);
				prio();
				}
				break;
			case T__0:
			case T__1:
			case T__2:
			case T__6:
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr1Context extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FacContext fac() {
			return getRuleContext(FacContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public Expr1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterExpr1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitExpr1(this);
		}
	}

	public final Expr1Context expr1() throws RecognitionException {
		Expr1Context _localctx = new Expr1Context(_ctx, getState());
		enterRule(_localctx, 12, RULE_expr1);
		try {
			setState(75);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				enterOuterAlt(_localctx, 1);
				{
				setState(67);
				match(T__5);
				setState(68);
				expr();
				setState(69);
				match(T__6);
				setState(70);
				fac();
				}
				break;
			case T__8:
			case T__9:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
				enterOuterAlt(_localctx, 2);
				{
				setState(72);
				number();
				setState(73);
				fac();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FacContext extends ParserRuleContext {
		public FacContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fac; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterFac(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitFac(this);
		}
	}

	public final FacContext fac() throws RecognitionException {
		FacContext _localctx = new FacContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_fac);
		try {
			setState(79);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__7:
				enterOuterAlt(_localctx, 1);
				{
				setState(77);
				match(T__7);
				}
				break;
			case T__0:
			case T__1:
			case T__2:
			case T__3:
			case T__4:
			case T__6:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public NonzeroNContext nonzeroN() {
			return getRuleContext(NonzeroNContext.class,0);
		}
		public List<NaturalContext> natural() {
			return getRuleContexts(NaturalContext.class);
		}
		public NaturalContext natural(int i) {
			return getRuleContext(NaturalContext.class,i);
		}
		public NonzeroHContext nonzeroH() {
			return getRuleContext(NonzeroHContext.class,0);
		}
		public List<HexaContext> hexa() {
			return getRuleContexts(HexaContext.class);
		}
		public HexaContext hexa(int i) {
			return getRuleContext(HexaContext.class,i);
		}
		public NonzeroBContext nonzeroB() {
			return getRuleContext(NonzeroBContext.class,0);
		}
		public List<BinaryContext> binary() {
			return getRuleContexts(BinaryContext.class);
		}
		public BinaryContext binary(int i) {
			return getRuleContext(BinaryContext.class,i);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitNumber(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_number);
		int _la;
		try {
			setState(116);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(81);
				nonzeroN();
				setState(85);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23))) != 0)) {
					{
					{
					setState(82);
					natural();
					}
					}
					setState(87);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 2);
				{
				setState(88);
				match(T__8);
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 3);
				{
				setState(89);
				match(T__9);
				setState(90);
				nonzeroH();
				setState(94);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__13) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28))) != 0)) {
					{
					{
					setState(91);
					hexa();
					}
					}
					setState(96);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 4);
				{
				setState(97);
				match(T__10);
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 5);
				{
				setState(98);
				match(T__11);
				setState(99);
				nonzeroB();
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__8 || _la==T__15) {
					{
					{
					setState(100);
					binary();
					}
					}
					setState(105);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 6);
				{
				setState(106);
				match(T__12);
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 7);
				{
				setState(107);
				match(T__13);
				setState(108);
				nonzeroB();
				setState(112);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__8 || _la==T__15) {
					{
					{
					setState(109);
					binary();
					}
					}
					setState(114);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 8);
				{
				setState(115);
				match(T__14);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NonzeroNContext extends ParserRuleContext {
		public NonzeroNContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nonzeroN; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterNonzeroN(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitNonzeroN(this);
		}
	}

	public final NonzeroNContext nonzeroN() throws RecognitionException {
		NonzeroNContext _localctx = new NonzeroNContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_nonzeroN);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NaturalContext extends ParserRuleContext {
		public NonzeroNContext nonzeroN() {
			return getRuleContext(NonzeroNContext.class,0);
		}
		public NaturalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_natural; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterNatural(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitNatural(this);
		}
	}

	public final NaturalContext natural() throws RecognitionException {
		NaturalContext _localctx = new NaturalContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_natural);
		try {
			setState(122);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(120);
				nonzeroN();
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 2);
				{
				setState(121);
				match(T__8);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NonzeroHContext extends ParserRuleContext {
		public NonzeroNContext nonzeroN() {
			return getRuleContext(NonzeroNContext.class,0);
		}
		public NonzeroHContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nonzeroH; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterNonzeroH(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitNonzeroH(this);
		}
	}

	public final NonzeroHContext nonzeroH() throws RecognitionException {
		NonzeroHContext _localctx = new NonzeroHContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_nonzeroH);
		try {
			setState(131);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(124);
				nonzeroN();
				}
				break;
			case T__24:
				enterOuterAlt(_localctx, 2);
				{
				setState(125);
				match(T__24);
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 3);
				{
				setState(126);
				match(T__13);
				}
				break;
			case T__25:
				enterOuterAlt(_localctx, 4);
				{
				setState(127);
				match(T__25);
				}
				break;
			case T__26:
				enterOuterAlt(_localctx, 5);
				{
				setState(128);
				match(T__26);
				}
				break;
			case T__27:
				enterOuterAlt(_localctx, 6);
				{
				setState(129);
				match(T__27);
				}
				break;
			case T__28:
				enterOuterAlt(_localctx, 7);
				{
				setState(130);
				match(T__28);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HexaContext extends ParserRuleContext {
		public NonzeroHContext nonzeroH() {
			return getRuleContext(NonzeroHContext.class,0);
		}
		public HexaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hexa; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterHexa(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitHexa(this);
		}
	}

	public final HexaContext hexa() throws RecognitionException {
		HexaContext _localctx = new HexaContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_hexa);
		try {
			setState(135);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__13:
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
				enterOuterAlt(_localctx, 1);
				{
				setState(133);
				nonzeroH();
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 2);
				{
				setState(134);
				match(T__8);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NonzeroBContext extends ParserRuleContext {
		public NonzeroBContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nonzeroB; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterNonzeroB(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitNonzeroB(this);
		}
	}

	public final NonzeroBContext nonzeroB() throws RecognitionException {
		NonzeroBContext _localctx = new NonzeroBContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_nonzeroB);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryContext extends ParserRuleContext {
		public NonzeroBContext nonzeroB() {
			return getRuleContext(NonzeroBContext.class,0);
		}
		public BinaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).enterBinary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof VCalculatorListener ) ((VCalculatorListener)listener).exitBinary(this);
		}
	}

	public final BinaryContext binary() throws RecognitionException {
		BinaryContext _localctx = new BinaryContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_binary);
		try {
			setState(141);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__8:
				enterOuterAlt(_localctx, 1);
				{
				setState(139);
				match(T__8);
				}
				break;
			case T__15:
				enterOuterAlt(_localctx, 2);
				{
				setState(140);
				nonzeroB();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3 \u0092\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\7\2\"\n\2\f\2\16"+
		"\2%\13\2\3\3\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5"+
		"\5\66\n\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7D\n\7\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bN\n\b\3\t\3\t\5\tR\n\t\3\n\3\n\7\nV\n"+
		"\n\f\n\16\nY\13\n\3\n\3\n\3\n\3\n\7\n_\n\n\f\n\16\nb\13\n\3\n\3\n\3\n"+
		"\3\n\7\nh\n\n\f\n\16\nk\13\n\3\n\3\n\3\n\3\n\7\nq\n\n\f\n\16\nt\13\n\3"+
		"\n\5\nw\n\n\3\13\3\13\3\f\3\f\5\f}\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r"+
		"\u0086\n\r\3\16\3\16\5\16\u008a\n\16\3\17\3\17\3\20\3\20\5\20\u0090\n"+
		"\20\3\20\2\2\21\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36\2\3\3\2\22\32\2"+
		"\u009d\2#\3\2\2\2\4&\3\2\2\2\6)\3\2\2\2\b\65\3\2\2\2\n\67\3\2\2\2\fC\3"+
		"\2\2\2\16M\3\2\2\2\20Q\3\2\2\2\22v\3\2\2\2\24x\3\2\2\2\26|\3\2\2\2\30"+
		"\u0085\3\2\2\2\32\u0089\3\2\2\2\34\u008b\3\2\2\2\36\u008f\3\2\2\2 \"\5"+
		"\4\3\2! \3\2\2\2\"%\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\3\3\2\2\2%#\3\2\2\2&"+
		"\'\5\6\4\2\'(\7\3\2\2(\5\3\2\2\2)*\5\n\6\2*+\5\b\5\2+\7\3\2\2\2,-\7\4"+
		"\2\2-.\5\n\6\2./\5\b\5\2/\66\3\2\2\2\60\61\7\5\2\2\61\62\5\n\6\2\62\63"+
		"\5\b\5\2\63\66\3\2\2\2\64\66\3\2\2\2\65,\3\2\2\2\65\60\3\2\2\2\65\64\3"+
		"\2\2\2\66\t\3\2\2\2\678\5\16\b\289\5\f\7\29\13\3\2\2\2:;\7\6\2\2;<\5\16"+
		"\b\2<=\5\f\7\2=D\3\2\2\2>?\7\7\2\2?@\5\16\b\2@A\5\f\7\2AD\3\2\2\2BD\3"+
		"\2\2\2C:\3\2\2\2C>\3\2\2\2CB\3\2\2\2D\r\3\2\2\2EF\7\b\2\2FG\5\6\4\2GH"+
		"\7\t\2\2HI\5\20\t\2IN\3\2\2\2JK\5\22\n\2KL\5\20\t\2LN\3\2\2\2ME\3\2\2"+
		"\2MJ\3\2\2\2N\17\3\2\2\2OR\7\n\2\2PR\3\2\2\2QO\3\2\2\2QP\3\2\2\2R\21\3"+
		"\2\2\2SW\5\24\13\2TV\5\26\f\2UT\3\2\2\2VY\3\2\2\2WU\3\2\2\2WX\3\2\2\2"+
		"Xw\3\2\2\2YW\3\2\2\2Zw\7\13\2\2[\\\7\f\2\2\\`\5\30\r\2]_\5\32\16\2^]\3"+
		"\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2aw\3\2\2\2b`\3\2\2\2cw\7\r\2\2de\7"+
		"\16\2\2ei\5\34\17\2fh\5\36\20\2gf\3\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2"+
		"\2jw\3\2\2\2ki\3\2\2\2lw\7\17\2\2mn\7\20\2\2nr\5\34\17\2oq\5\36\20\2p"+
		"o\3\2\2\2qt\3\2\2\2rp\3\2\2\2rs\3\2\2\2sw\3\2\2\2tr\3\2\2\2uw\7\21\2\2"+
		"vS\3\2\2\2vZ\3\2\2\2v[\3\2\2\2vc\3\2\2\2vd\3\2\2\2vl\3\2\2\2vm\3\2\2\2"+
		"vu\3\2\2\2w\23\3\2\2\2xy\t\2\2\2y\25\3\2\2\2z}\5\24\13\2{}\7\13\2\2|z"+
		"\3\2\2\2|{\3\2\2\2}\27\3\2\2\2~\u0086\5\24\13\2\177\u0086\7\33\2\2\u0080"+
		"\u0086\7\20\2\2\u0081\u0086\7\34\2\2\u0082\u0086\7\35\2\2\u0083\u0086"+
		"\7\36\2\2\u0084\u0086\7\37\2\2\u0085~\3\2\2\2\u0085\177\3\2\2\2\u0085"+
		"\u0080\3\2\2\2\u0085\u0081\3\2\2\2\u0085\u0082\3\2\2\2\u0085\u0083\3\2"+
		"\2\2\u0085\u0084\3\2\2\2\u0086\31\3\2\2\2\u0087\u008a\5\30\r\2\u0088\u008a"+
		"\7\13\2\2\u0089\u0087\3\2\2\2\u0089\u0088\3\2\2\2\u008a\33\3\2\2\2\u008b"+
		"\u008c\7\22\2\2\u008c\35\3\2\2\2\u008d\u0090\7\13\2\2\u008e\u0090\5\34"+
		"\17\2\u008f\u008d\3\2\2\2\u008f\u008e\3\2\2\2\u0090\37\3\2\2\2\20#\65"+
		"CMQW`irv|\u0085\u0089\u008f";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}