grammar VCalculator;

calculate 
    : calc*
    ;
calc 
    : expr ';'
    ;

expr 
    : first second
    ;
second 
    : '+' first second 
    | '-' first second
    |
    ;
first 
    : expr1 prio
    ;
prio 
    : '*' expr1 prio 
    | '/' expr1 prio
    |
    ;
expr1 
    : '(' expr ')' fac 
    | number fac
    ;
fac 
    : '!'
    |
    ;
number 
    : nonzeroN (natural)* 
    | '0' 
    | '0x' nonzeroH (hexa)* 
    | '0x0' 
    | 'b' nonzeroB (binary)* 
    | 'b0' 
    | 'B' nonzeroB (binary)* 
    | 'B0'
    ;
nonzeroN 
    : '1' 
    | '2' 
    | '3'
    | '4'
    | '5' 
    | '6'
    | '7' 
    | '8' 
    | '9'
    ;
natural 
    : nonzeroN 
    | '0'
    ;
nonzeroH 
    : nonzeroN 
    | 'A' 
    | 'B' 
    | 'C' 
    | 'D' 
    | 'E' 
    | 'F'
    ;
hexa 
    : nonzeroH 
    | '0'
    ;
nonzeroB 
    : '1'
    ;
binary 
    : '0' 
    | nonzeroB;
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines