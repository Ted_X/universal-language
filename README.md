# MENU
## [Jak program spustit?](jak-program-spustit?)
## [Prvky jazyka](#prvky-jazyka)
## [Operandy](#operandy)
## [Priorita](#priorita)
## [Příklady jazyka](#příklady-jazyka)
## [Gramatika](#gramatika)
## [Testy](#testy)

# Překladače
## Jak program spustit?
1. Naklonovat projekt
2. Spouštím program poprvé?
    - ANO 
        - v souboru ./LexikalniAnalyzator/startCalculator.sh změňte první řádek "python3 setup.py" na "python3 firstStart.py".
        - v souboru ./LexikalniAnalyzator/firstStart.py změňte řádek 10 a 14 na cestu do adresáře s projektem
    - NE 
        - ponechejte soubor beze změny
        - v souboru ./LexikalniAnalyzator/setup.py změňte řádek 7 a 11 na cestu do adresáře s projektem
3.  Jděte do složky ./LexikalniAnalyzator
4.  Spusťte program příkazem ./startCalculator.sh
5.  Při použití firstStart.py budete dotázání na heslo k počítači
6.  Nejprve se zobrazí stromová struktura
7.  Pro pokračování JEDNOU stiskněte v příkazovém řádku CTRL+C a zobrazí se výsledky příkladů
## Prvky jazyka
### Zadané prvky
1. **+** prvek pro sčítání
2. **\*** prvek pro násobení
2. **(** a **)** prvek začátku a konce závorky.

### Přidané prvky
1. **!** prvek pro faktoriál
2. **-** prvek pro odčítání

## Operandy
### Přirozená kladná čísla
- **Nulová hodnota**   -   0   ->   **\^[0]+$**
- **Nenulová hodnota**   -   1,2,3,4,5,6,7,8,9   ->   **\^[1-9]+$**
- **Číslo**   -   *Nulová hodnota* nebo *Nenulová hodnota*   ->   **^\d{1}$**
- **Čísla**   -   *Číslo**   ->   **\^[1-9]+[0-9]\*$**

### Hexadecimální kladná čísla
- **Hexadecimální číslo**   -   0^[xX]*Hexadecimální nenulová hodnota** | *Hexadecimální nulová* hodnota*
- **Hexadecimální nulová hodnota**   -   0x0
- **Hexadecimální nenulová hodnota**   -   1,2,3,4,5,6,7,8,9,A,B,C,D,E,F

### Binární kladná čísla
- **Binární číslo**   -   [b|B]*Binární hodnota**
- **Binární hodnota**   -   0,1

## Priorita
- Závorky, respektive výrazy v nich, mají největší prioritu
- operátor **\*** má větší prioritu než-li operátor **+**
- operátor **+** má stejnou prioritu jako operátor **-**

## Příklady jazyka
- 3+5
- 5-2
- 100*(6-5)
- 3!+4
- 2!+(101!-(5!+2!)!*3!)!
- bx0
- bx100110
- Bx10
- 0x45A2
- Bx1010!+0xA21/2671*0x0!

## Gramatika
- syntax = expr;
- expr = first, second;
- second = "+", first, second | "-", first, second|;
- first = expr1, prio;
- prio = "*", expr1, prio | "/", expr1, prio|;
- expr1 = "(", expr , ")", fac | number, fac;
- fac = "!"|;
- number = nonzeroN , {natural} | "0" | "0x", nonzeroH, {hexa} | "0x0" | "b", nonzeroB, {binary} | "b0" | "B", nonzeroB, {binary} | "B0";
- nonzeroN = "1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9";
- natural = nonzeroN | "0";
- nonzeroH = nonzeroN | "A" | "B" | "C" | "D" | "E" | "F";
- hexa =  nonzeroH | "0";
- nonzeroB = "1";
- binary = "0" | "nonzeroB";

## Testy
| Vstup | výsledek |
| ----------- | ----------- |
| 0 | 0 |
| 2+7 | 9 |
| 10-4 | 6 |
| 3*2+4-8 | 2 |
| 0! | 1 |
| 4!-3 | 21 |
| 4!-(1*4!) | 0 |
| 2!+(15-(9-4)+2)*4 | 50 |
| 0xA | 10 |
| 0xA131B+0x4 | 660255 |
| (0xB1-2)*4! | 4200 |
| 0x4A1-b101011 | 1201 |
| b1001 | 9 |
| B10 | 2 |
| b10111001/B101 | 37 |
| b0! | 1 |
| -5 | *error* |
| 20+(-6) | *error* |
| 6*(4-2 | *error* |
| 5+-4 | *error* |
| 8* | *error* |
| 0x01 | *error* |
| 0xb10 | *error* |
| 10*! | *error* |
